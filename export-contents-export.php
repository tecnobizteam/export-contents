<?php

require( '../../../wp-load.php' );

global $wpdb;

$posts = $wpdb->get_results("
  SELECT
    ID,
    post_title,
    post_content,
    post_date
  FROM
    {$wpdb->posts}
  WHERE
    post_status = 'publish'
    AND post_type IN (
      'imprensa',
      'artigos',
      'cronicas',
      'dicas',
      'entrevistas',
      'materias',
      'novidades',
      'podcasts',
      'videos',
      'whitepapers'
    )
    AND post_date > '2014-11-30'
    ORDER BY post_date",
    ARRAY_A
);

$now = gmdate("D, d M Y H:i:s");
header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
header("Last-Modified: {$now} GMT");

// force download
header("Content-Type: application/force-download");
header("Content-Type: application/octet-stream");
header("Content-Type: application/download");

// disposition / encoding on response body
header('Content-Encoding: UTF-8');
header('Content-type: text/csv; charset=UTF-8');
header("Content-Disposition: attachment;filename=export-contents.csv");
header("Content-Transfer-Encoding: binary");

$handle = fopen(
    'php://output',
    'w'
);

$defaultSeparator = array(
    'sep=,'
);
fputcsv($handle,$defaultSeparator);

$headers = array(
    'URL', 'Título', 'Texto', 'Data'
);

$header_archive = array();

foreach ($headers as $h){
  $header_column = mb_convert_encoding($h, 'UTF-16LE', 'UTF-8');
  $header_archive[] = $header_column;
}

fputcsv($handle,$header_archive);

foreach ( $posts as $k => $post ) {
    $post_export = array();
    $permalink = get_permalink( $post['ID'] );

    $post_export[] = $permalink;
    foreach ( $post as $key => $field ) {
        if ($key == 'ID')
            continue;
      $field_new = mb_convert_encoding($field, 'UTF-16LE', 'UTF-8');
        $post_export[] = strip_tags($field_new);
        $post[$key] = '';
    }

    $posts[$k] = '';
    fputcsv($handle,$post_export);
}

fclose( $handle );
die;