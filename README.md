# Export Contents
[![N|Solid](https://www.serasaexperian.com.br/wp-content/themes/serasaexperian/images/logo.png)](https://www.serasaexperian.com.br/)

Plugin para exportação de posts do wordpress.

# As funcionalidades deste plugin incluem
  - Exportar: Url, Título, Data e Link dos posts do Wordpress

# Instalação
  - Adicionar o arquivo 'export-contents.zip' à pasta '/wp-content/plugins/'.
  - Descompactar o arquivo e ativar o plugin.

# Exportação
 - No menu lateral da administração clique no item que aparece a engrenagem com nome: Export Contents;
 - Clique em exportar e baixe o csv.
  



