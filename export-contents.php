<?php

/*
Plugin Name: Export Contents
Description: Plugin para exportação de posts do wordpress.
Version: 1.0.0
Author: Cleiton @ tecnobiz
*/

add_action('activated_plugin', 'awesomeExportPosts');

function load_options_page()
{
    add_menu_page(
        'Export Contents',
        'Export Contents',
        'manage_options',
        'export-contents',
        'create_export_options_page'
    );
}

function create_export_options_page()
{
    include('custom-export-options-page.php');
}

add_action( 'admin_menu', 'load_options_page' );
